**Readme for RDC showcase 2024**
This repository contains the content presented in May 2024 for the McMaster RDC showcase. 

Contents:
		Slides: showcase-pres-15-mins.pdf

		Supporting material: showcase pres 15 mins.rmd; CRDCNtheme; content(logo; SHS updates); this readme

Status:

		License: This project is licensed under Creative Commons 4.0 CC-BY-NC. More details are available here: https://creativecommons.org/licenses/by-nc/4.0/

		Updates: There are no anticipated updates for this project.